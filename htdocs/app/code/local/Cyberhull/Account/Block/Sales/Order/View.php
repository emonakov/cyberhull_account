<?php

/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

class Cyberhull_Account_Block_Sales_Order_View extends Mage_Sales_Block_Order_View
{
    /**
     * Return back url for logged in and guest users
     *
     * @return string
     */
    public function getBackUrl()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getUrl('sales/order/history');
        }
        return Mage::getUrl('sales/order/form');
    }
}
<?php

/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

class Cyberhull_Account_Block_Customer_Form_Register extends Mage_Customer_Block_Form_Register
{
    protected function _prepareLayout()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn())
            $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('Create New Customer Account'));
        return parent::_prepareLayout();
    }
}

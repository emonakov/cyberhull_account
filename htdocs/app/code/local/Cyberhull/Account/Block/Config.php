<?php

class Cyberhull_Account_Block_Config extends Mage_Core_Block_Template
{
    protected $_configPrefix = 'cyberhull_account/ajax_account/';
    protected $_enabled;

    protected function _construct()
    {
        parent::_construct();
        $this->_enabled = (bool) Mage::getStoreConfig($this->_configPrefix . 'enabled');
    }

    protected function _toHtml()
    {
        if (!$this->_enabled) return '';
        return parent::_toHtml();
    }

    public function getJsonConfig()
    {
        $enabled = (bool) $this->_enabled;
        $contentContainer = Mage::getStoreConfig($this->_configPrefix . 'content_container_selector');
        $navigationContainer = Mage::getStoreConfig($this->_configPrefix . 'navigation_container_selector');
        $navLinksSelector = Mage::getStoreConfig($this->_configPrefix . 'navigation_links_selector');
        $accountContentContainer = Mage::getStoreConfig($this->_configPrefix . 'my_account_content_container');
        $excludedParentTags = explode(',', Mage::getStoreConfig($this->_configPrefix . 'excluded_parent_tags'));
        $excludedParentClasses = explode(',', Mage::getStoreConfig($this->_configPrefix . 'excluded_parent_classes'));
        $result = compact('enabled', 'contentContainer', 'navigationContainer', 'navLinksSelector', 'accountContentContainer', 'excludedParentTags', 'excludedParentClasses');
        return Mage::helper('core')->jsonEncode($result);
    }
}
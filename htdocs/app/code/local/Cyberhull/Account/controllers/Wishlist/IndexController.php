<?php
/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

require_once Mage::getModuleDir('controllers', 'Mage_Wishlist').DS.'IndexController.php';

class Cyberhull_Account_Wishlist_IndexController extends Mage_Wishlist_IndexController
{
    /**
     * Display customer wishlist
     */
    public function indexAction()
    {
        if ($this->getRequest()->isAjax()) {
            $this->loadLayout();

            $session = Mage::getSingleton('customer/session');
            $block   = $this->getLayout()->getBlock('customer.wishlist');
            $referer = $session->getAddActionReferer(true);
            if ($block) {
                $block->setRefererUrl($this->_getRefererUrl());
                if ($referer) {
                    $block->setRefererUrl($referer);
                }
            }

            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('checkout/session');
            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('wishlist/session');

            $this->renderLayout();
        } else {
            return parent::indexAction();
        }
    }

    /**
     * Update wishlist item comments
     */
    public function updateAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('wishlist/index/index'));
            }
            $wishlist = $this->_getWishlist();
            try {
                if (!$wishlist) {
                    throw new Mage_Core_Exception('wishlist not found');
                }
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('customer/session')->addError(
                    $this->__(Mage::helper('core')->escapeHtml($e->getMessages()))
                );
                return array('url' => Mage::getUrl('wishlist/index'));
            }

            $post = $this->getRequest()->getPost();
            if ($post && isset($post['description']) && is_array($post['description'])) {
                $updatedItems = 0;

                foreach ($post['description'] as $itemId => $description) {
                    $item = Mage::getModel('wishlist/item')->load($itemId);
                    if ($item->getWishlistId() != $wishlist->getId()) {
                        continue;
                    }

                    // Extract new values
                    $description = (string)$description;

                    if ($description == Mage::helper('wishlist')->defaultCommentString()) {
                        $description = '';
                    } elseif (!strlen($description)) {
                        $description = $item->getDescription();
                    }

                    $qty = null;
                    if (isset($post['qty'][$itemId])) {
                        $qty = $this->_processLocalizedQty($post['qty'][$itemId]);
                    }
                    if (is_null($qty)) {
                        $qty = $item->getQty();
                        if (!$qty) {
                            $qty = 1;
                        }
                    } elseif (0 == $qty) {
                        try {
                            $item->delete();
                        } catch (Exception $e) {
                            Mage::logException($e);
                            Mage::getSingleton('customer/session')->addError(
                                $this->__('Can\'t delete item from wishlist')
                            );
                        }
                    }

                    // Check that we need to save
                    if (($item->getDescription() == $description) && ($item->getQty() == $qty)) {
                        continue;
                    }
                    try {
                        $item->setDescription($description)
                            ->setQty($qty)
                            ->save();
                        $updatedItems++;
                    } catch (Exception $e) {
                        Mage::getSingleton('customer/session')->addError(
                            $this->__('Can\'t save description %s', Mage::helper('core')->escapeHtml($description))
                        );
                    }
                }

                // save wishlist model for setting date of last update
                if ($updatedItems) {
                    try {
                        $wishlist->save();
                        Mage::helper('wishlist')->calculate();
                    } catch (Exception $e) {
                        Mage::getSingleton('customer/session')->addError($this->__('Can\'t update wishlist'));
                    }
                }

                if (isset($post['save_and_share'])) {
                    return array('url' => Mage::getUrl('wishlist/index/share', array('wishlist_id' => $wishlist->getId())));
                }
            }
            return array('url' => Mage::getUrl('wishlist/index/index', array('wishlist_id' => $wishlist->getId())));
        } else {
            return parent::updateAction();
        }
    }

    /**
     * Remove item
     */
    public function removeAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('wishlist/index/index'));
            }
            try {
                $id = (int) $this->getRequest()->getParam('item');
                $item = Mage::getModel('wishlist/item')->load($id);
                if (!$item->getId()) {
                    throw new Mage_Core_Exception('wishlist item not found');
                }
                $wishlist = $this->_getWishlist($item->getWishlistId());
                if (!$wishlist) {
                    throw new Mage_Core_Exception('wishlist not found');
                }
                $item->delete();
                $wishlist->save();
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('customer/session')->addError(
                    $this->__('An error occurred while deleting the item from wishlist: %s', $e->getMessage())
                );
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError(
                    $this->__('An error occurred while deleting the item from wishlist.')
                );
            }

            Mage::helper('wishlist')->calculate();
            return array('url' => Mage::getUrl('wishlist/index/index'));
        } else {
            return parent::removeAction();
        }
    }

    /**
     * Share wishlist
     *
     * @return Mage_Core_Controller_Varien_Action|array
     */
    public function sendAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('wishlist/index/index'));
            }

            $wishlist = $this->_getWishlist();
            if (!$wishlist) {
                Mage::getSingleton('wishlist/session')->addError($this->__('wishlist not found'));
                return array('url' => Mage::getUrl('wishlist/index/index'));
            }

            $emails  = explode(',', $this->getRequest()->getPost('emails'));
            $message = nl2br(htmlspecialchars((string) $this->getRequest()->getPost('message')));
            $error   = false;
            if (empty($emails)) {
                $error = $this->__('Email address can\'t be empty.');
            }
            else {
                foreach ($emails as $index => $email) {
                    $email = trim($email);
                    if (!Zend_Validate::is($email, 'EmailAddress')) {
                        $error = $this->__('Please input a valid email address.');
                        break;
                    }
                    $emails[$index] = $email;
                }
            }
            if ($error) {
                Mage::getSingleton('wishlist/session')->addError($error);
                Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
                return array('url' => Mage::getUrl('wishlist/index/share', array('wishlist_id' => $wishlist->getId())));
            }

            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);

            try {
                $customer = Mage::getSingleton('customer/session')->getCustomer();

                /*if share rss added rss feed to email template*/
                if ($this->getRequest()->getParam('rss_url')) {
                    $rss_url = $this->getLayout()
                        ->createBlock('wishlist/share_email_rss')
                        ->setWishlistId($wishlist->getId())
                        ->toHtml();
                    $message .= $rss_url;
                }
                $wishlistBlock = $this->getLayout()->createBlock('wishlist/share_email_items')->toHtml();

                $emails = array_unique($emails);
                /* @var $emailModel Mage_Core_Model_Email_Template */
                $emailModel = Mage::getModel('core/email_template');

                $sharingCode = $wishlist->getSharingCode();
                foreach ($emails as $email) {
                    $emailModel->sendTransactional(
                        Mage::getStoreConfig('wishlist/email/email_template'),
                        Mage::getStoreConfig('wishlist/email/email_identity'),
                        $email,
                        null,
                        array(
                            'customer'       => $customer,
                            'salable'        => $wishlist->isSalable() ? 'yes' : '',
                            'items'          => $wishlistBlock,
                            'addAllLink'     => Mage::getUrl('wishlist/shared/allcart', array('code' => $sharingCode)),
                            'viewOnSiteLink' => Mage::getUrl('wishlist/shared/index', array('code' => $sharingCode)),
                            'message'        => $message
                        )
                    );
                }

                $wishlist->setShared(1);
                $wishlist->save();

                $translate->setTranslateInline(true);

                Mage::dispatchEvent('wishlist_share', array('wishlist' => $wishlist));
                Mage::getSingleton('customer/session')->addSuccess(
                    $this->__('Your Wishlist has been shared.')
                );
                return array('url' => Mage::getUrl('wishlist/index/index'));
            }
            catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('wishlist/session')->addError($e->getMessage());
                Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
                return array('url' => Mage::getUrl('wishlist/index/share', array('wishlist_id' => $wishlist->getId())));
            }
        } else {
            return parent::sendAction();
        }
    }
}

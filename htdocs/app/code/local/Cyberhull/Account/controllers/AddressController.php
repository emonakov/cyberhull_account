<?php
/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

require_once Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AddressController.php';

class Cyberhull_Account_AddressController extends Mage_Customer_AddressController
{
    /**
     * Customer addresses list
     */
    public function indexAction()
    {
        $this->loadLayout();
        if (count($this->_getSession()->getCustomer()->getAddresses())) {
            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('catalog/session');

            $block = $this->getLayout()->getBlock('address_book');
            if ($block) {
                $block->setRefererUrl($this->_getRefererUrl());
            }
        }
        $this->renderLayout();
    }

    public function editAction()
    {
        if ($this->getRequest()->isAjax()) {
            return array('url' => Mage::getUrl('customer/address/form', array('id'=> $this->getRequest()->getParam('id'))));
        }
        return parent::editAction();
    }

    public function newAction()
    {
        if ($this->getRequest()->isAjax()) {
            return array('url' => Mage::getUrl('customer/address/form'));
        }
        return parent::newAction();
    }

    public function formPostAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('customer/address/index'));
            }
            // Save data
            if ($this->getRequest()->isPost()) {
                $customer = $this->_getSession()->getCustomer();
                /* @var $address Mage_Customer_Model_Address */
                $address  = Mage::getModel('customer/address');
                $addressId = $this->getRequest()->getParam('id');
                if ($addressId) {
                    $existsAddress = $customer->getAddressById($addressId);
                    if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
                        $address->setId($existsAddress->getId());
                    }
                }

                $errors = array();

                /* @var $addressForm Mage_Customer_Model_Form */
                $addressForm = Mage::getModel('customer/form');
                $addressForm->setFormCode('customer_address_edit')
                    ->setEntity($address);
                $addressData    = $addressForm->extractData($this->getRequest());
                $addressErrors  = $addressForm->validateData($addressData);
                if ($addressErrors !== true) {
                    $errors = $addressErrors;
                }

                try {
                    $addressForm->compactData($addressData);
                    $address->setCustomerId($customer->getId())
                        ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
                        ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));

                    $addressErrors = $address->validate();
                    if ($addressErrors !== true) {
                        $errors = array_merge($errors, $addressErrors);
                    }

                    if (count($errors) === 0) {
                        $address->save();
                        $this->_getSession()->addSuccess($this->__('The address has been saved.'));
                        $reload = ($this->getRequest()->getParam('default_billing', false) || $this->getRequest()->getParam('default_shipping', false)) ? true : false;
                        return array('url' => Mage::getUrl('customer/address/index'), 'reload' => $reload);
                    } else {
                        $this->_getSession()->setAddressFormData($this->getRequest()->getPost());
                        foreach ($errors as $errorMessage) {
                            $this->_getSession()->addError($errorMessage);
                        }
                    }
                } catch (Mage_Core_Exception $e) {
                    $this->_getSession()->setAddressFormData($this->getRequest()->getPost())
                        ->addException($e, $e->getMessage());
                } catch (Exception $e) {
                    $this->_getSession()->setAddressFormData($this->getRequest()->getPost())
                        ->addException($e, $this->__('Cannot save address.'));
                }
            }
            return array('url' => Mage::getUrl('customer/address/index'));
        } else {
            return parent::formPostAction();
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('customer/address/index'));
            }
            $addressId = $this->getRequest()->getParam('id', false);

            if ($addressId) {
                $address = Mage::getModel('customer/address')->load($addressId);

                // Validate address_id <=> customer_id
                if ($address->getCustomerId() != $this->_getSession()->getCustomerId()) {
                    $this->_getSession()->addError($this->__('The address does not belong to this customer.'));
                    return array('url' => Mage::getUrl('customer/address/index'));
                }

                try {
                    $address->delete();
                    $this->_getSession()->addSuccess($this->__('The address has been deleted.'));
                } catch (Exception $e){
                    $this->_getSession()->addException($e, $this->__('An error occurred while deleting the address.'));
                }
            }
            return array('url' => Mage::getUrl('customer/address/index'));
        } else {
            return parent::deleteAction();
        }
    }
}
<?php
/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

class Cyberhull_Account_AjaxController extends Mage_Core_Controller_Front_Action
{
    /** @var  Mage_Core_Model_Url */
    protected $_urlModel;
    /** @var  Mage_Core_Controller_Varien_Front */
    protected $_front;
    /** @var  Mage_Core_Model_Config_Element */
    protected $_routes;
    protected $_params;
    protected $_reload = false;

    protected function _construct()
    {
        parent::_construct();
        $this->_front = Mage::app()->getFrontController();
        $this->_urlModel = Mage::getModel('core/url');
        $this->_routes = Mage::getConfig()->getNode('frontend/routers');
        $cache = Mage::app()->getCacheInstance();
        $cache->banUse('block_html');
        $cache->banUse('layout');
    }

    public function indexAction()
    {
        if (!$this->getRequest()->isAjax()) {
            $this->norouteAction();
            return false;
        }
        /**
         * Default customer account page
         */
        $response = [];
        $this->_params = $this->getRequest()->getParams();
        $controllerInstance = $this->_getControllerInstanceAndResult();
        $response['update_section']['content'] = $controllerInstance->getLayout()->getBlock('content')->toHtml();
        $response['update_section']['head'] = $controllerInstance->getLayout()->getBlock('head')->toHtml();
        $response['is_logged_in'] = Mage::getSingleton('customer/session')->isLoggedIn();
        $response['request_url'] = $this->_params['url'];
        $response['reload'] = $this->_reload;
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    protected function _getControllerInstanceAndResult()
    {
        $this->_params['url'] = urldecode($this->_params['url']);
        /** @var Mage_Core_Controller_Front_Action $controllerInstance */
        $controllerInstance = $this->_getController();
        $result = $this->_runInstance($controllerInstance);
        if (is_array($result) && isset($result['url'])) {
            $this->_params['url'] = $result['url'];
            if (isset($result['reload'])) {
                $this->_reload = $result['reload'];
            }
            return $this->_getControllerInstanceAndResult();
        }
        return $controllerInstance;
    }

    protected function _getController()
    {
        $this->_urlModel->parseUrl($this->_params['url']);
        $this->_urlModel->setRoutePath(trim($this->_urlModel->getPath(), '/'));
        $this->_urlModel->getQueryParams();
        if ($routeParams = $this->_urlModel->getRouteParams()) {
            $this->getRequest()->setParams($routeParams);
        }
        if ($queryParams = $this->_urlModel->getQueryParams()) {
            $this->getRequest()->setQuery($this->_urlModel->getQueryParams());
        }
        $routeName = $this->_urlModel->getRouteFrontName();
        $controllerName = $this->_urlModel->getControllerName();
        if (!$controllerName) {
            $controllerName = 'index';
            $this->_urlModel->setControllerName($controllerName);
        }
        /** @var Mage_Core_Controller_Varien_Router_Standard $router */
        $router = $this->_front->getRouterByFrontName($routeName);
        $modules = $router->getModuleByFrontName($routeName);
        $controllerClassName = null;
        $controllerFile = null;
        foreach ($modules as $module) {
            $class = $router->getControllerClassName($module, $controllerName);
            $file = $router->getControllerFileName($module, $controllerName);
            if (file_exists($file)) {
                $controllerClassName = $class;
                $controllerFile = $file;
                break;
            }
        }

        include_once $controllerFile;
        $controllerInstance = Mage::getControllerInstance(
            $controllerClassName,
            $this->getRequest(),
            $this->getResponse()
        );
        return $controllerInstance;
    }

    protected function _runInstance(Mage_Core_Controller_Front_Action $controller)
    {
        $controller->loadLayout();
        $this->_addLayoutUpdate();
        $action = $this->_getAction();
        $controller->preDispatch();
        $this->getRequest()->setRouteName($this->_urlModel->getRouteFrontName());
        $this->getRequest()->setControllerName($this->_urlModel->getControllerName());
        $this->getRequest()->setActionName($this->_getActionName());
        $result = $controller->$action();
        $controller->postDispatch();
        return $result;
    }

    protected function _getAction()
    {
        return $this->getActionMethodName($this->_getActionName());
    }

    protected function _getActionName()
    {
        $actionName = $this->_urlModel->getActionName();
        return ($actionName) ? $actionName : 'index';
    }

    protected function _getLayoutHandle()
    {
        return $this->_urlModel->getRouteFrontName() . '_' . $this->_urlModel->getControllerName() . '_' . $this->_getActionName();
    }

    protected function _addLayoutUpdate()
    {
        $this->getLayout()->getUpdate()->addHandle($this->_getLayoutHandle());
        return $this;
    }
}
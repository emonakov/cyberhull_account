<?php
/**
 * Cyberhull_Account
 *
 * @category   Cyberhull
 * @package    Cyberhull_Account
 * @author     Eugene Monakov <emonakov@robofirm.com>
 */

require_once Mage::getModuleDir('controllers', 'Mage_Newsletter').DS.'ManageController.php';

class Cyberhull_Account_Newsletter_ManageController extends Mage_Newsletter_ManageController
{
    public function saveAction()
    {
        if ($this->getRequest()->isAjax()) {
            if (!$this->_validateFormKey()) {
                return array('url' => Mage::getUrl('newsletter/manage/index'));
            }
            try {
                Mage::getSingleton('customer/session')->getCustomer()
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setIsSubscribed((boolean)$this->getRequest()->getParam('is_subscribed', false))
                ->save();
                if ((boolean)$this->getRequest()->getParam('is_subscribed', false)) {
                    Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been saved.'));
                } else {
                    Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been removed.'));
                }
            }
            catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($this->__('An error occurred while saving your subscription.'));
            }
            return array('url' => Mage::getUrl('newsletter/manage/index'));
        } else {
            return parent::saveAction();
        }
    }
}

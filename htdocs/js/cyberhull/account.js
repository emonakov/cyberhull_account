'use strict';
function AjaxAccount() {}
var ajaxAccount = function ($j, $, $$) {
    var pageContainer,
        accountNavigation,
        navigationLinksSelector,
        proxyNavigationUrl,
        body,
        head,
        loadOverlay,
        accountLinksSelector,
        accountFormSelector,
        exclusionParentClasses,
        exclusionParentTags;

    AjaxAccount.prototype.initialise = function () {
        pageContainer = $(ajaxAccountConfig.contentContainer);
        accountNavigation = $(ajaxAccountConfig.navigationContainer);
        navigationLinksSelector = ajaxAccountConfig.navLinksSelector + ' a';
        proxyNavigationUrl = accountNavigation.readAttribute('data-proxy-url');
        body = $j('body');
        head = $j('head');
        loadOverlay = $j('#loadOverlay');
        accountLinksSelector = ajaxAccountConfig.accountContentContainer + ' a';
        accountFormSelector = ajaxAccountConfig.accountContentContainer + ' form';
        exclusionParentClasses = ajaxAccountConfig.excludedParentClasses;
        exclusionParentTags = ajaxAccountConfig.excludedParentTags;
        this.initObservers();
    };

    AjaxAccount.prototype.initObservers = function () {
        /**
         * Main menu handler
         */
        Event.on(accountNavigation, 'click', navigationLinksSelector, function (event, element) {
            $j(accountNavigation).find('li.current').removeClass('current');
            $j(element).parent().addClass('current');
            this.makeSimpleAjaxCall(element);
            event.stop();
        }.bind(this));
        /**
         * Inner links handler
         */
        try {
            Event.on(pageContainer, 'click', accountLinksSelector, function (event, element) {
                var url = element.readAttribute('href');
                if (url && !this.isExcluded(element)) {
                    this.makeSimpleAjaxCall(element);
                    event.stop();
                }
            }.bind(this));
        } catch (e) {
            console.log(e);
        }
        /**
         * Forms handler
         */
        try {
            Event.on(pageContainer, 'submit', accountFormSelector, function (event, element) {
                var submitUrl = element.action;
                var method = element.method;
                var parameters = element.serialize(true);
                parameters.url = submitUrl;
                this.loadAjax({
                    url: proxyNavigationUrl,
                    method: method,
                    parameters: parameters,
                    onSuccessCallback: this.updateContent.bind(this)
                });
                event.stop();
            }.bind(this));
        } catch (e) {
            console.log(e);
        }
    };

    AjaxAccount.prototype.makeSimpleAjaxCall = function (element, target) {
        var url;
        if (element)
            url = element.readAttribute('href');
        if (target)
            url = target;
        var encodedUrl = encodeURIComponent(url);
        if (url == '#' || !url) {
            return false;
        }
        this.loadAjax({
            url: proxyNavigationUrl,
            parameters: {
                url: encodedUrl
            },
            onSuccessCallback: this.updateContent.bind(this)
        });
    };

    AjaxAccount.prototype.updateContent = function (resp) {
        var json = JSON.parse(resp.responseText);
        var html = json.update_section.content;
        window.history.pushState(null, '', json.request_url);
        var headHtml = json.update_section.head;
        $j(pageContainer).html(html);
        head.html(headHtml);
    };

    AjaxAccount.prototype.spinnerFadeIn = function () {
        loadOverlay.fadeIn(200, function () {
            body.css('overflow', 'hidden');
        });
        return this;
    };

    AjaxAccount.prototype.spinnerFadeOut = function () {
        loadOverlay.fadeOut(200, function () {
            body.css('overflow', '');
        });
        return this;
    };

    AjaxAccount.prototype.setParameters = function (parameters) {
        this.parameters = parameters;
        return this;
    };

    AjaxAccount.prototype.getParameters = function () {
        return this.parameters;
    };

    AjaxAccount.prototype.loadAjax = function (config) {
        var context = this;
        var reqMethod = config.method || 'post';
        var onLoading = function () {
            context.spinnerFadeIn();
            if (config.hasOwnProperty('onLoadCallback')) {
                config.onLoadCallback.apply(this, arguments);
            }
        };
        var onSuccess = function () {
            if (config.hasOwnProperty('onSuccessCallback')) {
                config.onSuccessCallback.apply(this, arguments);
            }
        };
        var onComplete = function (resp) {
            try {
                var json = JSON.parse(resp.responseText);
                if (json.reload) {
                    context.makeSimpleAjaxCall(null, json.request_url);
                    return false;
                }
            } catch (e) {
                location.reload();
            }
            context.spinnerFadeOut();
            if (config.hasOwnProperty('onCompleteCallback')) {
                config.onCompleteCallback.apply(this, arguments);
            }
        };
        var parameters = config.parameters || this.parameters;
        return new Ajax.Request(config.url, {
            method: reqMethod,
            onLoading: onLoading,
            onSuccess: onSuccess,
            onComplete: onComplete,
            parameters: parameters
        });
    };

    AjaxAccount.prototype.confirmRemoveWishlistItem = function (url) {
        if (confirmRemoveWishlistItem()) {
            if (url) {
                this.makeSimpleAjaxCall(null, url);
            }
        }
    };

    /**
     * RWD account and card dropdowns fix
     */
    AjaxAccount.prototype.rebindSkipItems = function () {
        var skipContents = $j('.skip-content');
        var skipLinks = $j('.skip-link');

        skipLinks.off('click');
        skipLinks.on('click', skipContentsLink);
        $j('#header-cart').off('click', '.skip-link-close');
        $j('#header-cart').on('click', '.skip-link-close', skipCartLink);
        function skipContentsLink (e) {
            e.preventDefault();

            var self = $j(this);
            // Use the data-target-element attribute, if it exists. Fall back to href.
            var target = self.attr('data-target-element') ? self.attr('data-target-element') : self.attr('href');

            // Get target element
            var elem = $j(target);

            // Check if stub is open
            var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

            // Hide all stubs
            skipLinks.removeClass('skip-active');
            skipContents.removeClass('skip-active');

            // Toggle stubs
            if (isSkipContentOpen) {
                self.removeClass('skip-active');
            } else {
                self.addClass('skip-active');
                elem.addClass('skip-active');
            }
        }

        function skipCartLink(e) {
            var parent = $j(this).parents('.skip-content');
            var link = parent.siblings('.skip-link');

            parent.removeClass('skip-active');
            link.removeClass('skip-active');

            e.preventDefault();
        }
    };

    AjaxAccount.prototype.isExcluded = function (element) {
        var result = false;
        exclusionParentTags.forEach(function (entry) {
            var $parent = $j(element).closest(entry);
            exclusionParentClasses.forEach(function (className) {
                if ($parent.hasClass(className)) {
                    result = true;
                }
            });
        });
        return result;
    };

    AjaxAccount.prototype.changePagerFunction = function () {
        var $pager = $j('.pager');
        var $pagerLimiter = $pager.find('.limiter select');
        $pagerLimiter.attr('onchange', 'ajaxAccount.makeSimpleAjaxCall(null, this.value)');
    };

    return new AjaxAccount();
}(jQuery, $, $$);

jQuery(function () {
    ajaxAccount.changePagerFunction();
});

document.observe("dom:loaded", function () {
    ajaxAccount.initialise();
});